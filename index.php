
<?php

$valid_passwords = array("test" => "12346789");
$valid_users = array_keys($valid_passwords);

$user_now = $_SERVER['PHP_AUTH_USER'];
$pass_now = $_SERVER['PHP_AUTH_PW'];

$validated = (in_array($user_now, $valid_users)) && ($pass_now == $valid_passwords[$user_now]);

if (!$validated) {
  header('WWW-Authenticate: Basic realm="TEST"');
  header('HTTP/1.0 401 Unauthorized');
  die("Nothing Here : " . IP);
}
if(isset($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 'home';
}
$ext = '.html';

echo file_get_contents('header.html');
echo file_get_contents("page_content/".$page.$ext);
echo file_get_contents('footer.html');

?>